package com.my.tengxuanmap.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;

import com.my.tengxuanmap.R;
import com.my.tengxuanmap.constant.TXMapConstant;
import com.my.tengxuanmap.util.NetWorkUtils;
import com.my.tengxuanmap.util.ToastUtil;
import com.tencent.map.geolocation.TencentLocation;
import com.tencent.map.geolocation.TencentLocationListener;
import com.tencent.map.geolocation.TencentLocationManager;
import com.tencent.map.geolocation.TencentLocationRequest;
import com.tencent.mapsdk.raster.model.CameraPosition;
import com.tencent.mapsdk.raster.model.LatLng;
import com.tencent.tencentmap.mapsdk.map.MapView;
import com.tencent.tencentmap.mapsdk.map.TencentMap;

public class SearchLocationActivity extends AppCompatActivity implements TencentLocationListener {
    private EditText etInput;
    private TencentLocationManager locationManager;
    private TencentLocationRequest locationRequest;
    private TencentMap tencentMap;
    private MapView mapView;
    private ImageView ivCurrentPostion;
    private Animation translateAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_location);
        /**
         * 业务逻辑:
         *
         * 1.每次打开此页面 定位用户当前的位置
         * 1.1 判断当前网络是否可用
         * 1.1.1 可用，检查定位权限
         * 1.1.1.1 拥有权限 进行定位操作
         * 1.1.1.2 没有前线 弹窗 到“设置”进行操作
         * 1.1.2 不可用 toast 提示 “当前网络不可用”
         *
         * 2.点击按钮显示当前位置
         * 同上逻辑
         *
         * 3.移动地图 CameraChange getCenter 保留用户当前位置Marker; 显示当前位置
         */

        initView();
        initListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(this);
    }

    private void initView() {
        locationManager = TencentLocationManager.getInstance(this);
        locationRequest = TencentLocationRequest.create();
        mapView = findViewById(R.id.search_map);
        tencentMap = mapView.getMap();
        ivCurrentPostion = findViewById(R.id.iv_current_search);
        etInput = findViewById(R.id.et_input_search);

        translateAnimation = AnimationUtils.loadAnimation(this, R.anim.view_animation);
    }

    private void initListener() {
        findViewById(R.id.iv_location_serach).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPosition();
                ivCurrentPostion.startAnimation(translateAnimation);

            }
        });

        tencentMap.setOnMapCameraChangeListener(new TencentMap.OnMapCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

            }

            @Override
            public void onCameraChangeFinish(CameraPosition cameraPosition) {
                Log.i("mwb", "当前地图的中心点 经度:" + tencentMap.getMapCenter().getLongitude());
                Log.i("mwb", "当前地图的中心点 维度:" + tencentMap.getMapCenter().getLatitude());

//                if (NetWorkUtils.isNetworkAvailable(SearchLocationActivity.this)) {
//                    setMarkerPostion(tencentMap.getMapCenter().getLongitude(), tencentMap.getMapCenter().getLatitude());
//                } else {
//                    showToast(TXMapConstant.NETWORK_UNAVAILABLE);
//                }
            }

        });
    }

    /**
     * 设置当前位置
     */
    private void setPosition() {
        if (NetWorkUtils.isNetworkAvailable(this)) { // 当前网络可用
            checkMyPermission(); // 检查权限
        } else { // 当前网络不可用
            ToastUtil.showShort(this, TXMapConstant.NETWORK_UNAVAILABLE);
        }
    }

    private void checkMyPermission() {
        // 1.判断是否拥有定位的权限
        // 1.1 拥有权限进行相应操作
        // 1.2 没有权限申请权限
        // 1.2.1 Android 6.0 动态申请权限
        // 1.2.1.1 用户给予权限进行相应操作
        // 1.2.1.2 用户没有给予权限 作出相应提示
        // 1.2.2 某些5.0权限的手机执行相应操作
        String[] permission = {
                Manifest.permission.ACCESS_COARSE_LOCATION
        };

        if (ContextCompat.checkSelfPermission(this, permission[0]) == PackageManager.PERMISSION_GRANTED) { // 拥有权限
            getMyLocation();
        } else { // 没有权限
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { //6.0
                requestPermissions(permission, 0);
            } else {
                // 此处为某些5.0动态权限的手机
                tipPermissionAlertDialog();
            }
        }
    }

    /**
     * 获取当前定位
     */
    private void getMyLocation() {
        int error = locationManager.requestLocationUpdates(
                locationRequest, this);

        switch (error) {
            case 0:
                Log.i("mwb", "成功注册监听器");
                break;
            case 1:
                Log.i("mwb", "设备缺少使用腾讯定位服务需要的基本条件");
                break;
            case 2:
                Log.i("mwb", "manifest 中配置的 key 不正确");
                break;
            case 3:
                Log.i("mwb", "自动加载libtencentloc.so失败");
                break;
            default:
                break;
        }
    }

    private void tipPermissionAlertDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("提示");
        builder.setMessage(TXMapConstant.SET_LOCATION_PERMISSION);
        builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlertDialog alertDialog = builder.show();
                alertDialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onLocationChanged(TencentLocation tencentLocation, int i, String s) {
        locationManager.removeUpdates(this); // 定位完成之后关闭定位，等待再次触发
        LatLng currentPostion;
        currentPostion = new LatLng(tencentLocation.getLatitude(),
                tencentLocation.getLongitude());
        tencentMap.animateTo(currentPostion); // 动画移动至经纬度
        tencentMap.setZoom(18); // 设置地图缩放比例
    }

    @Override
    public void onStatusUpdate(String s, int i, String s1) {

    }
}

package com.my.tengxuanmap.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.my.tengxuanmap.R;

public class IndexActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        initListener();
    }

    private void initListener() {
        findViewById(R.id.btn_simple).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goIntent(MainActivity.class);
            }
        });

        findViewById(R.id.btn_search_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goIntent(SearchLocationActivity.class);
            }
        });
    }

    /**
     * 跳转Activity
     *
     * @param cla
     */
    private void goIntent(Class cla) {
        startActivity(new Intent(this, cla));
    }
}

package com.my.tengxuanmap.constant;

/**
 * 项目名称：TengXunMap 常量
 * 创建人：mwb
 * 创建时间：2018/9/3 9:34
 */
public class TXMapConstant {
    /**
     * 当前网络不可用
     */
    public static final CharSequence NETWORK_UNAVAILABLE = "当前网络不可用，请检查网络设置";

    /**
     * 位置权限
     */
    public static final String SET_LOCATION_PERMISSION = "请在“设置”中给予“TengXunMap”位置权限,否则当前功能将不可使用";

}
